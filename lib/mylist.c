#include <stdlib.h>
#include <stdio.h>

#include "mylist.h"

void mylist_init(struct mylist* l)
{
  l->head = NULL;
}

void mylist_destroy(struct mylist* l)
{
  while(l->head != NULL)
  {
    mylist_remove(l, mylist_get_head(l));
  }

}

void mylist_insert(struct mylist* l, struct mylist_node* before, int data)
{
  struct mylist_node* N;
  N = (struct mylist_node*)malloc(sizeof(struct mylist_node));
  N->data = data;

  if(before == NULL)
  {
    N->next = l->head;
    l->head = N;
  }
  else
  {
    N->next = before->next;
    before->next = N;
  }
}

void mylist_remove(
    struct mylist* l,
    struct mylist_node* target)
{
  struct mylist_node* N;

  if(target == l->head)
  {
    N = l->head->next;
    free(l->head);
    l->head = N;
  }
  else
  {
    N = l->head;
    while(N->next != target)
    {
      N = N->next;
    }
    N->next = target->next;
    free(target);
  }
}

struct mylist_node* mylist_find(struct mylist* l, int target)
{
  struct mylist_node* N = l->head;
  while(N != NULL)
  {
    if(N->data == target)
    {
      return N;
    }
    N = N->next;
  }
  return NULL; // If not found
}

struct mylist_node* mylist_get_head(struct mylist* l)
{
  return l->head;
}

void mylist_print(const struct mylist* l)
{
  for (struct mylist_node* pointer = l->head;
      pointer != NULL;
      pointer = pointer->next)
  {
    printf("%d\n", pointer->data);
  }
}
